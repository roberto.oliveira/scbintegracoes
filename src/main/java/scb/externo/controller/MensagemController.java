package scb.externo.controller;



import org.eclipse.jetty.http.HttpStatus;

import io.javalin.http.Context;
import scb.externo.model.Mensagem;
import scb.externo.repository.MensagemRepository;
import scb.externo.service.ServicoEmail;

public final class MensagemController {

	private MensagemController() {
		
	}
	
	public static void enviaEmail(Context ctx) {
		Mensagem.MensagemJson jsonM;
		try {
			jsonM = ctx.bodyAsClass(Mensagem.MensagemJson.class);
		} catch (Exception e) {
			ctx.status(HttpStatus.BAD_REQUEST_400);
			ctx.result("bad input parameter");
			return;
		}

		Mensagem email = new Mensagem(jsonM);
		MensagemRepository.insert(email);
		try {
			ServicoEmail envioEmail = new ServicoEmail();
			envioEmail.enviarEmail(email);
			email.setStatusEnviada();
			MensagemRepository.update(email);
			ctx.status(HttpStatus.OK_200);
			ctx.result("altered status");
		} catch (Exception ex) {
			email.setStatusErro();
			MensagemRepository.update(email);
			ctx.status(HttpStatus.BAD_REQUEST_400);
			ctx.result("email not sent");
		}
	}
	
}
