package scb.externo.controller;

import org.eclipse.jetty.http.HttpStatus;

import io.javalin.http.Context;
import scb.externo.model.CartaoCredito;
import scb.externo.model.Transacao;
import scb.externo.repository.TransacaoRepository;
import scb.externo.service.ServicoCobranca;

public final class CobrancaController {
	
	private CobrancaController(){
		
	}
	
	public static void cobraCartao (Context ctx) {
		CartaoCredito jsonCC;
		try {
			jsonCC = ctx.bodyAsClass(CartaoCredito.class);
		} catch (Exception e) {
			ctx.status(HttpStatus.BAD_REQUEST_400);
			ctx.result("bad input parameter");
			return;
		}

		Transacao transacao = new Transacao(jsonCC);
		TransacaoRepository.insert(transacao);
		ServicoCobranca cobranca = new ServicoCobranca();

		if (cobranca.cobrarTransacao(transacao))
			ctx.status(HttpStatus.OK_200);
		else
			ctx.status(HttpStatus.BAD_REQUEST_400);
		ctx.result(transacao.getMensagemRequisicao());
		TransacaoRepository.update(transacao);
	}

}
