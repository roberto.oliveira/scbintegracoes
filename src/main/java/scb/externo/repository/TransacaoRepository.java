package scb.externo.repository;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import scb.externo.model.Transacao;

public final class TransacaoRepository {
	
	private TransacaoRepository() {
		
	}
	
	private static final Map<UUID,Transacao> transacoes = new HashMap<>();
	
	public static void insert(Transacao t)  {
		if (t.getIdPedido() == null)
			t.setIdPedido(UUID.randomUUID());
		else if (transacoes.containsKey(t.getIdPedido()))
			throw new IllegalArgumentException("ja existe esta transacao");
		transacoes.put(t.getIdPedido(), t);
	}
	
	public static void update(Transacao t)  {
		if (t.getIdPedido() == null)
			insert(t);
		else
			transacoes.put(t.getIdPedido(), t);
	}
	
	public static Transacao findByUUID(UUID id) {
		return transacoes.get(id);
	}

}
