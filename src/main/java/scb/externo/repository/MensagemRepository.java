package scb.externo.repository;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import scb.externo.model.Mensagem;

public final class MensagemRepository{
	
	private MensagemRepository() {
		
	}
	
	private static final Map<UUID,Mensagem> mensagens = new HashMap<>();
	
	public static void insert(Mensagem m)  {
		if (m.getIdMensagem() == null)
			m.setIdMensagem(UUID.randomUUID());
		else if (mensagens.containsKey(m.getIdMensagem()))
			throw new IllegalArgumentException("Mensagem duplicada");
		mensagens.put(m.getIdMensagem(),m);
	}
	
	public static void update(Mensagem m)  {
		if (m.getIdMensagem() == null)
			insert(m);
		else
			mensagens.put(m.getIdMensagem(),m);
	}
	
	public static Mensagem findByUUID(UUID id) {
		return mensagens.get(id);
	}

}
