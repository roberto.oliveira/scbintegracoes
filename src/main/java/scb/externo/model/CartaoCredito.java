package scb.externo.model;

public class CartaoCredito {
	
	private String numero;
	private String proprietario;
	private String validade;
	private String codigo;
	private String cpfProprietario;
	private EnderecoCobranca enderecoCobranca;
	private double valorCobranca;
	
	public CartaoCredito(){
		
	}

	public CartaoCredito(String numero, String proprietario, String validade, String codigo,
			String cpfProprietario, EnderecoCobranca enderecoCobranca, double valorCobranca)  
					throws IllegalArgumentException {
		super();
		this.numero = numero;
		this.proprietario = proprietario;
		this.validade = validade;
		this.codigo = codigo;
		this.cpfProprietario = cpfProprietario;
		this.enderecoCobranca = enderecoCobranca;
		validaValorCobranca(valorCobranca);
		this.valorCobranca = valorCobranca;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getProprietario() {
		return proprietario;
	}

	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}

	public String getValidade() {
		return validade;
	}

	public void setValidade(String validade) {
		this.validade = validade;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getCpfProprietario() {
		return cpfProprietario;
	}

	public void setCpfProprietario(String cpfProprietario) {
		this.cpfProprietario = cpfProprietario;
	}

	public EnderecoCobranca getEnderecoCobranca() {
		return enderecoCobranca;
	}

	public void setEnderecoCobranca(EnderecoCobranca enderecoCobranca) {
		this.enderecoCobranca = enderecoCobranca;
	}

	public double getValorCobranca() {
		return valorCobranca;
	}

	public void setValorCobranca(double valorCobranca) throws IllegalArgumentException {
		validaValorCobranca(valorCobranca);
		this.valorCobranca = valorCobranca;
	}
	
	static void validaValorCobranca(Double valor) {
		if (valor <= 0)
			throw new IllegalArgumentException("Valor inválido");
	}

}
