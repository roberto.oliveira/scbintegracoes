package scb.externo.model;

import java.util.Date;
import java.util.UUID;

public class Transacao {
	
	private enum statusTransacao {
		PENDENTE, APROVADA, NEGADA;
	}
	
	private UUID idPedido;
	private Date dataRequisicao;
	private statusTransacao status;
	private Date dataAprovacao;
	private CartaoCredito cartao;
	private String mensagemRequisicao;
	
	public Transacao(){

	}
	
	public Transacao(CartaoCredito cartao) {
		super();
		this.cartao = cartao;
		this.dataRequisicao = new Date();
		this.status = statusTransacao.PENDENTE;
	}

	public UUID getIdPedido() {
		return idPedido;
	}

	public void setIdPedido(UUID idPedido) {
		this.idPedido = idPedido;
	}

	public Date getDataRequisicao() {
		return dataRequisicao;
	}

	public void setDataRequisicao(Date dataRequisicao) {
		this.dataRequisicao = dataRequisicao;
	}

	public statusTransacao getStatus() {
		return status;
	}

	public void setStatusAprovada() {
		this.status = status.APROVADA;
	}
	
	public void setStatusNegada() {
		this.status = status.NEGADA;
	}

	public Date getDataAprovacao() {
		return dataAprovacao;
	}

	public void setDataAprovacao(Date dataAprovacao) {
		this.dataAprovacao = dataAprovacao;
	}

	public CartaoCredito getCartao() {
		return cartao;
	}

	public void setCartao(CartaoCredito cartao) {
		this.cartao = cartao;
	}

	public String getMensagemRequisicao() {
		return mensagemRequisicao;
	}

	public void setMensagemRequisicao(String mensagemRequisicao) {
		this.mensagemRequisicao = mensagemRequisicao;
	}

	

}
