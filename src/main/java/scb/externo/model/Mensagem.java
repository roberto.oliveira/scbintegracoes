package scb.externo.model;

import java.util.Date;
import java.util.UUID;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

public class Mensagem {
	
	private enum statusMensagem {
		CONSTRUCAO, ENVIADA, ERRO;
	}
	
	private UUID idMensagem;
	private final String nomeRemetente = "Sistema de Biciletas";
	private final String enderecoRementente = "naoresponder.scb@gmail.com";
	private String nomeDestinatario;
	private InternetAddress enderecoDestinatario;
	private String assunto;
	private String conteudo;
	private Date dataSolicitacao;
	private Date ultimaAlteracao;
	private statusMensagem status;

	public Mensagem() {
		
	}

	public Mensagem(InternetAddress enderecoDestinatario, String conteudo, String assunto) {
		super();
		this.enderecoDestinatario = enderecoDestinatario;
		this.conteudo = conteudo;
		this.status = statusMensagem.CONSTRUCAO;
		this.dataSolicitacao = new Date();
		this.assunto = assunto;
	}
	
	public Mensagem(String email, String conteudo, String assunto) throws AddressException {
		this.enderecoDestinatario = new InternetAddress(email, true);
		this.conteudo = conteudo;
		this.status = statusMensagem.CONSTRUCAO;
		this.dataSolicitacao = new Date();
		this.assunto = assunto;
	}
	
	public Mensagem(MensagemJson m) {
		this(m.email, m.mensagem, m.assunto);
	}
	
	
	
	public UUID getIdMensagem() {
		return idMensagem;
	}

	public void setIdMensagem(UUID idMensagem) {
		this.idMensagem = idMensagem;
	}

	public String getNomeDestinatario() {
		return nomeDestinatario;
	}

	public void setNomeDestinatario(String nomeDestinatario) {
		this.nomeDestinatario = nomeDestinatario;
	}

	public InternetAddress getEnderecoDestinatario() {
		return enderecoDestinatario;
	}

	public void setEnderecoDestinatario(InternetAddress enderecoDestinatario) {
		this.enderecoDestinatario = enderecoDestinatario;
	}

	public void setEnderecoDestinatario(String enderecoDestinatario) throws AddressException {
		this.enderecoDestinatario = new InternetAddress(enderecoDestinatario, true);
	}
	
	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	public Date getDataSolicitacao() {
		return dataSolicitacao;
	}

	public void setDataSolicitacao(Date dataSolicitacao) {
		this.dataSolicitacao = dataSolicitacao;
	}

	public Date getUltimaAlteracao() {
		return ultimaAlteracao;
	}

	public void setUltimaAlteracao(Date ultimaAlteracao) {
		this.ultimaAlteracao = ultimaAlteracao;
	}

	public statusMensagem getStatus() {
		return status;
	}

	public void setStatusEnviada() {
		this.status = statusMensagem.ENVIADA;
	}
	
	public void setStatusErro() {
		this.status = statusMensagem.ERRO;
	}
	

	public String getNomeRemetente() {
		return nomeRemetente;
	}

	public String getEnderecoRementente() {
		return enderecoRementente;
	}
	
	public static class MensagemJson {
		
		InternetAddress email;
		String assunto;
		String mensagem;
		
		public MensagemJson() {
			/*
			 * Para uso do Javalin
			 */
		}
		
		public InternetAddress getEmail() {
			return email;
		}
		
		public void setEmail (String email) throws AddressException {
			this.email = new InternetAddress(email, true);
		}
		
		public String getMensagem() {
			return mensagem;
		}
		
		public void setMensagem(String mensagem) {
			this.mensagem = mensagem;
		}
		
		public void setAssunto(String a) {
			this.assunto = a;
		}
		
		public String getAssunto() {
			return this.assunto;
		}
		
		
	}

	
	
}
