package scb.externo.model.request;

import scb.externo.model.CartaoCredito;

public class CreditCard {
	
	private String CardNumber;
	private String Holder;
	private String ExpirationDate;
	private String SecurityCode;
	private final String SaveCard = "false";
	private final String Brand = "Visa";
	
	public CreditCard(CartaoCredito cc) {
		this.CardNumber = cc.getNumero();
		this.Holder = cc.getProprietario();
		this.ExpirationDate = cc.getValidade();
		this.SecurityCode = cc.getCodigo();
	}
	
	public CreditCard() {
		//para json
	}

	public String getCardNumber() {
		return CardNumber;
	}

	public void setCardNumber(String cardNumber) {
		CardNumber = cardNumber;
	}

	public String getHolder() {
		return Holder;
	}

	public void setHolder(String holder) {
		Holder = holder;
	}

	public String getExpirationDate() {
		return ExpirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		ExpirationDate = expirationDate;
	}

	public String getSecurityCode() {
		return SecurityCode;
	}

	public void setSecurityCode(String securityCode) {
		SecurityCode = securityCode;
	}

	public String getSaveCard() {
		return SaveCard;
	}

	public String getBrand() {
		return Brand;
	}

	
}
