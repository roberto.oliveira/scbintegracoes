package scb.externo.model.request;

import scb.externo.model.Transacao;

public class Payment {
	
	private final String Type = "CreditCard";
	private double Amount;
	private final String Currency = "BRL";
	private final String Country = "BRA";
	private final String Provider = "Simulado";
	private final int ServiceTaxAmount = 0;
	private final int Installments = 1;
	private final String Interest = "ByMerchant";
	private final boolean Capture = false;
	private final boolean Authenticate = false;
	private final boolean Recurrent = false;
	private final String SoftDescriptor = "123456789ABCD";
	private CreditCard CreditCard;
	
	public Payment(Transacao t) {
		Amount = t.getCartao().getValorCobranca()*100;
		CreditCard = new CreditCard(t.getCartao());
	}
	
	public Payment() {
		//para json
	}

	public double getAmount() {
		return Amount;
	}

	public void setAmount(double amount) {
		Amount = amount;
	}

	public CreditCard getCreditCard() {
		return CreditCard;
	}

	public void setCreditCard(CreditCard creditCard) {
		CreditCard = creditCard;
	}

	public String getType() {
		return Type;
	}

	public String getCurrency() {
		return Currency;
	}

	public String getCountry() {
		return Country;
	}

	public String getProvider() {
		return Provider;
	}

	public int getServiceTaxAmount() {
		return ServiceTaxAmount;
	}

	public int getInstallments() {
		return Installments;
	}

	public String getInterest() {
		return Interest;
	}

	public boolean isCapture() {
		return Capture;
	}

	public boolean isAuthenticate() {
		return Authenticate;
	}

	public boolean isRecurrent() {
		return Recurrent;
	}

	public String getSoftDescriptor() {
		return SoftDescriptor;
	}
	
	
	
}

