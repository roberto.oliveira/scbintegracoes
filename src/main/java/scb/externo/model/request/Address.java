package scb.externo.model.request;

import scb.externo.model.EnderecoCobranca;

public class Address {
    private String Street;
    private String Number;
    private String Complement;
    private String ZipCode;
    private String City;
    private String State;
    private String Country;
    
    public Address(EnderecoCobranca ec) {
    	this.Street = ec.getRua();
    	this.Number = ec.getNumero();
    	this.Complement = ec.getComplemento();
    	this.ZipCode = ec.getCep();
    	this.State = ec.getEstado();
    	this.Country = ec.getPais();
    }
    
    public Address() {
    	//para json
    }

	public String getStreet() {
		return Street;
	}

	public void setStreet(String street) {
		Street = street;
	}

	public String getNumber() {
		return Number;
	}

	public void setNumber(String number) {
		Number = number;
	}

	public String getComplement() {
		return Complement;
	}

	public void setComplement(String complement) {
		Complement = complement;
	}

	public String getZipCode() {
		return ZipCode;
	}

	public void setZipCode(String zipCode) {
		ZipCode = zipCode;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

}
