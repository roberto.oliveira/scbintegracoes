package scb.externo.model.request;

import scb.externo.model.Transacao;

public class RequisicaoCobranca {
	private String MerchantOrderId;
	private Customer Customer;
	private Payment Payment;
	
	public RequisicaoCobranca(Transacao t) {
		MerchantOrderId = t.getIdPedido().toString();
		Customer = new Customer(t);
		Payment = new Payment(t);
	}
	
	public RequisicaoCobranca() {
		//para json
	}

	public String getMerchantOrderId() {
		return MerchantOrderId;
	}

	public void setMerchantOrderId(String merchantOrderId) {
		MerchantOrderId = merchantOrderId;
	}

	public Customer getCustomer() {
		return Customer;
	}

	public void setCustomer(Customer customer) {
		Customer = customer;
	}

	public Payment getPayment() {
		return Payment;
	}

	public void setPayment(Payment payment) {
		Payment = payment;
	}
	
	
}
