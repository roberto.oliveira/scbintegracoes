package scb.externo.model.request;

import scb.externo.model.Transacao;

public class Customer {
	
	private String Name;
	private String Identity;
	private final String IdentityType = "CPF";
	private final String Email = "roberto.oliveira@uniriotec.br";
	private final String BirthDate = "1991-01-02";
	private Address Address;
	private Address DeliveryAddress;
	
	public Customer(Transacao t) {
		Name = t.getCartao().getProprietario();
		Identity = t.getCartao().getCpfProprietario();
		Address = new Address(t.getCartao().getEnderecoCobranca());
		DeliveryAddress = Address;
	}
	
	public Customer() {
		//para json
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getIdentity() {
		return Identity;
	}

	public void setIdentity(String identity) {
		Identity = identity;
	}

	public Address getAddress() {
		return Address;
	}

	public void setAddress(Address address) {
		Address = address;
	}

	public Address getDeliveryAddress() {
		return DeliveryAddress;
	}

	public void setDeliveryAddress(Address deliveryAddress) {
		DeliveryAddress = deliveryAddress;
	}

	public String getIdentityType() {
		return IdentityType;
	}

	public String getEmail() {
		return Email;
	}

	public String getBirthDate() {
		return BirthDate;
	}
	
	
}