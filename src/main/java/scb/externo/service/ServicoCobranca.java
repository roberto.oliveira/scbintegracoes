package scb.externo.service;

import java.util.Date;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;
import scb.externo.configapi.CieloAPI;
import scb.externo.model.Transacao;
import scb.externo.model.request.RequisicaoCobranca;

public class ServicoCobranca {

	
	public boolean cobrarTransacao(Transacao t) {
		RequisicaoCobranca reqCobranca = new RequisicaoCobranca(t);
		HttpResponse<String> response = Unirest.post(CieloAPI.getApiUrl()+"/1/sales")
				  .header("Content-Type", "application/json")
				  .header("MerchantId", CieloAPI.getMerchantId())
				  .header("MerchantKey", CieloAPI.getMerchantKey())
				  .body(reqCobranca)
				  .asString();
		if (response.getStatus() == 201) {
			JSONObject resposta = new JSONObject(response.getBody());
			JSONObject pagamento = resposta.getJSONObject("Payment");
			t.setMensagemRequisicao(pagamento.getString("ReturnMessage"));
			if (pagamento.getString("ReturnCode").equals("4") || pagamento.getString("ReturnCode").equals("6")) {
				t.setStatusAprovada();
				t.setDataAprovacao(new Date());
				return true;
			}
			t.setStatusNegada();
			return true;
		}
		t.setMensagemRequisicao("Bad input parameter");
		return false;
	}
}
