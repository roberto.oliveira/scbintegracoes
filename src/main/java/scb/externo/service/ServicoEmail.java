package scb.externo.service;

import scb.externo.model.Mensagem;

import java.util.Date;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage; 
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

public final class ServicoEmail {
	
	private static final String ENDERECOREMETENTE = "naoresponder.scb@gmail.com";
	private static final String SENHA = "qnkzjcmpwbwiugdv";		
	private static InternetAddress emailRemetente;
    private static Properties props = new Properties();
    private static Session sessao;
	
	final class Credencial extends Authenticator {

		PasswordAuthentication credenciais = new PasswordAuthentication(ENDERECOREMETENTE,SENHA);
		
		Credencial(){
			
		}
		
		@Override
		protected PasswordAuthentication getPasswordAuthentication() {
			return credenciais;
		}	
	}

	
	public ServicoEmail() throws AddressException {
		setProps();
		emailRemetente = new InternetAddress(ENDERECOREMETENTE, true);
		sessao = Session.getDefaultInstance(props,new Credencial());
	}
	
	private static void setProps() {
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.ssl.checkserveridentity", "true");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");
	}
	
	
	public void enviarEmail(Mensagem m) throws MessagingException {
		Message email = new MimeMessage(sessao);
		email.setFrom(emailRemetente);
		email.setRecipient(Message.RecipientType.TO, m.getEnderecoDestinatario());
		email.setSubject(m.getAssunto());
		email.setText(m.getConteudo());
		m.setUltimaAlteracao(new Date());
		Transport.send(email);
	}
}
