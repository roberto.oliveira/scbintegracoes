package scb.externo;


import io.javalin.Javalin;
import io.javalin.http.Context;
import scb.externo.controller.CobrancaController;
import scb.externo.controller.MensagemController;

import static io.javalin.apibuilder.ApiBuilder.*;

public class Application {
	private static final int DEFAULT_PORT = 7000;
	
	public static void main(String[] args) {
		int port = DEFAULT_PORT;
		if (System.getenv("PORT") != null) {
			try {
				port = Integer.valueOf(System.getenv("PORT"));
			} catch (NumberFormatException ex) {

			}
		}
		Javalin.create(config -> {
			config.defaultContentType = "application/json";
			config.enableCorsForAllOrigins();
		}).routes(() -> {
			path("", () -> get(Application::serverStatus));
			path("cobraCartao", () -> post(CobrancaController::cobraCartao));
			path("enviaEmail", () -> post(MensagemController::enviaEmail));
		}).start(port);
	}

	private static void serverStatus(Context ctx) {
		String result = "Aplicação rodando";
		ctx.result(result);
	}
}

