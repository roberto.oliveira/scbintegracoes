package scb.externo.repository;

import static org.junit.jupiter.api.Assertions.*;

import javax.mail.internet.AddressException;

import org.junit.jupiter.api.Test;

import scb.externo.model.Mensagem;

class MensagemRepositoryTest {
	
	private static final String enderecoDestinatario = "beto@gmail.com";
	private static final String conteudo = "Teste de conteudo";
	private static final String assunto = "Teste de assunto";
	private Mensagem email;
	
	MensagemRepositoryTest() throws AddressException {
		this.email = new Mensagem(enderecoDestinatario, conteudo, assunto);
	}
	
	@Test
	void mensagemRepositoryInsertFindTest() {
		MensagemRepository.insert(email);
		assertNotNull(email.getIdMensagem());
		assertEquals(MensagemRepository.findByUUID(email.getIdMensagem()), email);
		assertEquals(MensagemRepository.findByUUID(email.getIdMensagem()).getConteudo(), email.getConteudo());
	}
	
	@Test
	void mensagemRepositoryUpdateFindTest() {
		MensagemRepository.update(email);
		assertNotNull(email.getIdMensagem());
		email = MensagemRepository.findByUUID(email.getIdMensagem());
		String conteudoNovo = "Novo teste";
		email.setConteudo(conteudoNovo);
		MensagemRepository.update(email);
		assertEquals(MensagemRepository.findByUUID(email.getIdMensagem()).getConteudo(), conteudoNovo);
	}
	
	@Test
	void mensagemRepositoryInsertDuplicate() {
		MensagemRepository.insert(email);
		assertThrows(IllegalArgumentException.class,
				() -> MensagemRepository.insert(email));
		
	}
}
