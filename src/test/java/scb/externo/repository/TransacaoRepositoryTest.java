package scb.externo.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import scb.externo.model.CartaoCredito;
import scb.externo.model.EnderecoCobranca;
import scb.externo.model.Transacao;

class TransacaoRepositoryTest {
	
    	private final String numero = "12345678901234";
    	private final String proprietario = "Jose das couves";
    	private final String validade = "10/2020";
    	private final String codigo = "231";
    	private final String cpfProprietario = "99999999999";
    	private final EnderecoCobranca enderecoCobranca = new EnderecoCobranca("Rua das amoras", 
    			"114", "apartamento 319", "Frutas", "São Gonçalo", "RJ", "23221-999"); 
    	private final double valorCobranca = 25.51;
    	private final CartaoCredito cartao = new CartaoCredito(numero, proprietario, validade,
    			codigo, cpfProprietario, enderecoCobranca, valorCobranca);
    	private Transacao transacao = new Transacao(cartao);
	
	@Test
	void transacaoRepositoryInsertFindTest(){
		TransacaoRepository.insert(transacao);
		assertNotNull(transacao.getIdPedido());
		assertEquals(TransacaoRepository.findByUUID(transacao.getIdPedido()), transacao);
	}
	
	@Test
	void transacaoRepositoryUpdateFindTest() {
		TransacaoRepository.update(transacao);
		assertNotNull(transacao.getIdPedido());
		transacao = TransacaoRepository.findByUUID(transacao.getIdPedido());
		Double novoValorCobranca = 43.00;
		transacao.getCartao().setValorCobranca(novoValorCobranca);
		TransacaoRepository.update(transacao);
		assertEquals(TransacaoRepository.findByUUID(transacao.getIdPedido()).getCartao().getValorCobranca(), novoValorCobranca);
	}
	
	@Test
	void transacaoRepositoryInsertDuplicate() {
		TransacaoRepository.insert(transacao);
		assertThrows(IllegalArgumentException.class,
				() -> TransacaoRepository.insert(transacao));
		
	}

}
