package scb.externo.model;

import static org.junit.jupiter.api.Assertions.*;

import javax.mail.internet.AddressException;

import org.junit.jupiter.api.Test;


class MensagemTest {
	
	
	String emailDestinatario = "xxxyyy";
	String conteudo = "conteudo do email";
	String assunto = "assunto do email";
	
	@Test
	void insertNewEmailTest() {
		assertThrows(AddressException.class,
				() -> new Mensagem(emailDestinatario, conteudo, assunto));
	}

	@Test
	void updateEmail() {
		Mensagem msg = new Mensagem();
		assertThrows(AddressException.class,
				() -> msg.setEnderecoDestinatario(emailDestinatario));
	}

}
