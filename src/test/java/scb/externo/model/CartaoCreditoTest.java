package scb.externo.model;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class CartaoCreditoTest {
	
	@Test
	void testValorCobrancaNegativo() {
		assertThrows(IllegalArgumentException.class,
				() -> CartaoCredito.validaValorCobranca(-5.0));
	}
	
	@Test
	void testValorCobranca() {
		double valor = 35.00;
		CartaoCredito cartao = new CartaoCredito();
		cartao.setValorCobranca(valor);
		assertEquals(valor, cartao.getValorCobranca());
	}

}
